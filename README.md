
## Requirements

Julia
  
  - Only Julia 1.5 is required (other packages will be installed automatically).

Python
  
  - anaconda3
  - [pyjulia](https://github.com/JuliaPy/pyjulia) (run `python -c "import julia; julia.install()"` to install PyCall.jl)
  - PyTorch 1.7
  - geopandas


## Map Matching

Navigate to folder `mm` and setup the map matching.

## Traffic Tensor Generation

```bash
cd jucode && bash run.sh
```

## Model Running

```bash
cd pycode
export JULIA_LOAD_PATH=../mm/entrance/client/src/:$JULIA_LOAD_PATH
export JULIA_LOAD_PATH=../jucode/src/:$JULIA_LOAD_PATH
python train.py -use_selu
python evaluate.py
```