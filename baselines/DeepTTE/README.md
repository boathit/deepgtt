## Requirement

* Julia 1.5
* Python 2.7

## Data Generation

```
bash data_gen.sh
```

## Run

```
bash run.sh
```