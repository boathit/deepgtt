using Dates, Statistics, JSON, ProgressMeter
using Trips: Trip, readtripsjld2

inputpath = "../../mm/trips/output"
outputpath = "data"
train_names = ["gps_20161101.jld2", "gps_20161102.jld2", "gps_20161103.jld2", 
               "gps_20161104.jld2", "gps_20161105.jld2", "gps_20161106.jld2"]
valid_names = ["gps_20161107.jld2"]
test_names = ["gps_20161108.jld2"]

function discretize_min(dt::DateTime, slotsize=1)
    hour, minute = Dates.hour(dt), Dates.minute(dt)
    div(hour*60 + minute, slotsize)
end

function trip2dict(trip::Trip)
    dt = unix2datetime(trip.tms[1])
    Dict(
        :lats=>trip.lat,
        :lngs=>trip.lon,
        :states=>zeros(size(trip.lon)),
        :time=>trip.tms[end]-trip.tms[1],
        :time_gap=>vcat([0], trip.tms[2:end] - trip.tms[1:end-1]),
        :driverID=>0,
        :dateID=>dayofmonth(dt),
        :weekID=>dayofweek(dt) - 1,
        :timeID=>discretize_min(dt),
        :dist=>sum(trip.spdist),
        :dist_gap=>cumsum(trip.spdist),
    )
end


let (lat_min, lat_max) = (90., -90.)
    (lon_min, lon_max) = (180., -180.)
    stat_keys = [:lats, :lngs, :time, :time_gap, :dist, :dist_gap]
    summary_stats = Dict(
        :lats_mean=>[],
        :lats_std=>[],
        :lngs_mean=>[],
        :lngs_std=>[],
        :time_mean=>[],
        :time_std=>[],
        :time_gap_mean=>[],
        :time_gap_std=>[],
        :dist_mean=>[],
        :dist_std=>[],
        :dist_gap_mean=>[],
        :dist_gap_std=>[]
    )

    for name in readdir(inputpath)
        println("$(name)...")
        stats = Dict(
            :lats=>[],
            :lngs=>[],
            :time=>[],
            :time_gap=>[],
            :dist=>[],
            :dist_gap=>[]
        )
        trips = readtripsjld2(joinpath(inputpath, name))
        dicts = map(trip2dict, trips)
        open(joinpath(outputpath, name |> splitext |> first), "w") do f
            @showprogress for (trip, dict) in zip(trips, dicts)
                if (trip.validspeed && trip.tms[end]-trip.tms[1] >= 7*60 
                    && length(trip.lon) >= 5 && length(trip.cpath) >= 5)
                    write(f, JSON.json(dict) * "\n")
                end
                lat_min = min(lat_min, minimum(trip.lat))
                lat_max = max(lat_max, maximum(trip.lat))
                lon_min = min(lon_min, minimum(trip.lon))
                lon_max = max(lon_max, maximum(trip.lon))
            end
        end
        if name in train_names || name in valid_names
            @showprogress for (trip, dict) in zip(trips, dicts)
                if (trip.validspeed && trip.tms[end]-trip.tms[1] >= 7*60 
                    && length(trip.lon) >= 5 && length(trip.cpath) >= 5)
                    for stat_key in stat_keys
                        append!(stats[stat_key], dict[stat_key])
                    end
                end
            end
            for stat_key in stat_keys
                push!(summary_stats[Symbol("$(stat_key)_mean")], mean(stats[stat_key]))
                push!(summary_stats[Symbol("$(stat_key)_std")], std(stats[stat_key]))
            end
        end
    end
    summary_dict = Dict()
    for key in keys(summary_stats)
        summary_dict[key] = mean(summary_stats[key])
    end
    summary_dict[:train_set] = map(x -> splitext(x) |> first, train_names)
    summary_dict[:eval_set] = map(x -> splitext(x) |> first, valid_names)
    summary_dict[:test_set] = map(x -> splitext(x) |> first, test_names)

    open("config.json", "w") do f
        #write(f, JSON.json(summary_dict))
        JSON.print(f, summary_dict, 2)
    end
    bound_dict = Dict(
        :lat_min=>lat_min,
        :lat_max=>lat_max,
        :lon_min=>lon_min,
        :lon_max=>lon_max
    )
    JSON.print(stdout, summary_dict, 2)
    JSON.print(stdout, bound_dict, 2)
end
# "lon_max": 104.12705396396409,
# "lat_max": 30.730172870508863,
# "lon_min": 104.03968949529664,
# "lat_min": 30.65539983491892