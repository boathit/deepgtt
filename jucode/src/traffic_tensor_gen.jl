using Trips
using SpatialRegion
using FileIO

inputpath = "/home/xiucheng/Github/mm/trips/output"
outputpath = "../../data/output"
region = Region("chengdu", 104.038, 30.654, 104.128, 30.731, 200.0, 200.0)
traffic_tensor = Dict{Tuple, Matrix}()
for file in readdir(inputpath)
    println("$(file)...")
    trips = readtripsjld2(joinpath(inputpath, file))
    merge!(traffic_tensor, create_traffic_tensors(region, trips))
end

FileIO.save(joinpath(outputpath, "traffic_tensor.jld2"), "traffic_tensor", traffic_tensor)

# "lon_max": 104.12705396396409,
# "lat_max": 30.730172870508863,
# "lon_min": 104.03968949529664,
# "lat_min": 30.65539983491892