
import torch, os
import numpy as np
import ast
from torchtext import legacy
from collections import namedtuple
from typing import Any, List, Optional, Tuple
from geopandas import GeoDataFrame
from toolz.curried import *
from torch.utils.data import Dataset
from datetime import datetime
from sklearn.preprocessing import MinMaxScaler

from julia.api import Julia
jl = Julia(compiled_modules=False)
from julia.Trips import readtripsjld2
from julia.SpatialRegion import tms2key
from julia import FileIO

Trip = namedtuple("Trip", ["cpath", "index", "ratio", "tms", "key"])
TrainBatch = namedtuple("TrainBatch", ["S", "cpath", "route_len", "ratio", "time", "stms"])
## be used to shift edge ids and leave id space for padding
SHIFT = 1

class TripDataset(Dataset):
    def __init__(self, dirpath: str, names: List[str]) -> None:
        if len(names) == 0:
            names = [file for file in os.listdir(dirpath)]
        self.trips = []
        for name in names:
            print(f"Loading {name}")
            self.trips.extend(self.readtrips(dirpath, name))
        print(f"Loaded {len(self.trips)} trips.")

    def __len__(self):
        return len(self.trips)

    def __getitem__(self, idx):
        if torch.is_tensor(idx): idx = idx.to_list()
        return self.trips[idx]
    
    @staticmethod
    def readtrips(dirpath: str, name: str) -> List[Trip]:
        trips = readtripsjld2(os.path.join(dirpath, name))
        f = lambda trip: trip.validspeed and (trip.tms[-1]-trip.tms[0] >= 7*60)\
                and len(trip.lon) >= 5 and len(trip.cpath) >= 5
        return [Trip(cpath=trip.cpath,
                     index=trip.index,
                     ratio=trip.ratio,
                     tms=trip.tms,
                     key=tms2key(trip.tms[1])) for trip in trips if f(trip)]

def get_dataloader_from_dataset(
    dataset: Dataset, 
    batch_size: int, 
    train: bool, 
    device: Optional[torch.device]=torch.device("cpu")
) -> Any:
    return legacy.data.BucketIterator(
        dataset, batch_size,
        sort_key=lambda x: len(x.cpath),
        train=train,
        shuffle=True,
        repeat=True,
        sort=False,
        sort_within_batch=True,
        device=device
    )

def get_dataloader(
    dirpath: str,
    names: List[str],
    batch_size: int,
    train: bool,
    device: Optional[torch.device]=torch.device("cpu")
) -> Any:
    dataset = TripDataset(dirpath, names)
    return get_dataloader_from_dataset(
        dataset,
        batch_size,
        train,
        device
    )
    
def discretize_min(tms: float, slotsize: int) -> int:
    dt = datetime.utcfromtimestamp(tms)
    return (dt.hour*60 + dt.minute) // slotsize

def get_transform_batch(traffic_tensor_file: str) -> Any:
    """
    Return `transform_batch` which transforms the dataloader returned batch to
    model required batch.
    """  
    traffic_tensor = FileIO.load(traffic_tensor_file, "traffic_tensor")

    def padxs(xs, PAD):
        def padx(x, maxlen, PAD):
            return np.concatenate((x, [PAD]*(maxlen - len(x))))
        lens = list(map(len, xs))
        maxlen = max(lens)
        xs = [padx(xs[i], maxlen, PAD) for i in range(len(xs))]
        xs = np.stack(xs).astype(np.int)
        return xs, lens

    def transform_batch(batch: List[Trip]) -> TrainBatch:
        ε = 1e-9
        S = np.stack([get(trip.key, traffic_tensor) for trip in batch])
        cpath, route_len = padxs([trip.cpath for trip in batch], -1)
        # shift all edges by one and leave 0 for PAD
        cpath = cpath + SHIFT
        ratio = np.stack([[1-trip.ratio[0], trip.ratio[-1]] for trip in batch])
        time = [trip.tms[-1] - trip.tms[0] for trip in batch]
        stms = [trip.tms[0] for trip in batch]
        return TrainBatch(
            S=torch.FloatTensor(S).unsqueeze_(1),
            cpath=torch.LongTensor(cpath),
            route_len=torch.LongTensor(route_len).reshape(-1, 1),
            ratio=torch.FloatTensor(ratio)+ε,
            time=torch.FloatTensor(time)/60,
            stms=stms
        )
    
    return transform_batch

def split_batch(batch: List[Trip]) -> Tuple[List[Trip], List[Trip]]:
    """
    Split a batch of trips into two batches, batch1, batch2. batch1 containes
    the first half of trips, batch2 containes the last half of trips.
    """
    def left_trip(trip: Trip) -> Trip:
        n = len(trip.index) // 2
        index = trip.index[:n+1]
        cpath = trip.cpath[index[0]:index[-1]+1]
        ratio = trip.ratio[:n+1]
        tms = trip.tms[:n+1]
        key = trip.key
        return Trip(cpath=cpath, index=index, ratio=ratio, tms=tms, key=key)
    def right_trip(trip: Trip) -> Trip:
        n = len(trip.index) // 2
        index = trip.index[n:]
        cpath = trip.cpath[index[0]:index[-1]+1]
        ratio = trip.ratio[n:]
        tms = trip.tms[n:]
        key = trip.key
        return Trip(cpath=cpath, index=index, ratio=ratio, tms=tms, key=key)
    
    return list(map(left_trip, batch)), list(map(right_trip, batch))

def get_highwayids(edges: GeoDataFrame) -> Tuple[np.array, int]:
    """
    Return `hids` that maps an edge id to its highway type id.
    It assumes that the edges are ordered by their ids in `edges`.

    Returns:
        hids (1+num_edge,)
        num_h

    hids, num_h = get_highwayids(edges)
    """
    edges["refined_highway"] = edges.highway.map(
        lambda x: first(ast.literal_eval(x)) if x.startswith('[') else x
    )
    mapd = {
        "residential": 0,
        "tertiary": 1,
        "unclassified": 2,
        "primary": 3,
        "secondary": 4,
        "living_street": 5,
        "trunk": 6,
        "secondary_link": 7,
        "trunk_link": 8,
        "primary_link": 9,
        "tertiary_link": 10,
        "motorway_link": 11,
        "motorway": 12,
        "service": 13,
        "road": 14 # unknown type
    }
    edges["hid"] = edges.refined_highway.map(
        lambda x: get(x, mapd, 14)
    )
    # shift all edges by one and leave 0 for PAD
    return np.r_[[0]*SHIFT, edges.hid.values], len(mapd)

def get_centroids(edges: GeoDataFrame) -> np.array:
    """
    Return `centroids` that maps an edge id to its centroid coordinate.
    It assumes that the edges are ordered by their ids in `edges`.

    Args:
        edges: osm data.
        epsg: different regions should set different epsg, for China, epsg=4490.

    Returns:
       centroids (1+num_edge, 2)

    centroids = get_centroid_map(edges)
    """
    scaler = MinMaxScaler()
    coords = pipe(edges.geometry.to_crs(epsg=3395)
                  .centroid.map(lambda p: (p.x, p.y)).values, 
                  map(list), list, np.array,
                  scaler.fit_transform)
    # shift all edges by one and leave 0 for PAD
    return np.r_[np.zeros((SHIFT, 2)), coords]

def get_lengths(edges: GeoDataFrame) -> np.array:
    """
    Return `lengths` that maps an edge id to its length.
    It assumes that the edges are ordered by their ids in `edges`.

    Returns:
        lengths (1+num_edge,)
    """
    lengths = edges.geometry.to_crs(epsg=3395).length.values
    lengths = lengths / lengths.max()
    # shift all edges by one and leave 0 for PAD
    return np.r_[[0.]*SHIFT, lengths]