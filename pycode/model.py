
from typing import List, Optional, Tuple
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch import FloatTensor, LongTensor
from typing import Optional, Tuple
from datautils import TrainBatch, discretize_min
from toolz.curried import *
from collections import namedtuple

class MLP(nn.Module):
    def __init__(
        self, 
        input_size: int, 
        hidden_size: int, 
        output_size: int,
        dropout: float, 
        use_selu: Optional[bool]=False
    ) -> None:
        super(MLP, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, output_size)
        self.nonlinear_f = F.selu if use_selu else F.leaky_relu
        self.dropout = nn.Dropout(dropout)
    def forward(self, x: FloatTensor) -> FloatTensor:
        h1 = self.dropout(self.nonlinear_f(self.fc1(x)))
        return self.fc2(h1)

class MLP2(nn.Module):
    """
    MLP with two output layers
    """
    def __init__(
        self, 
        input_size: int, 
        hidden_size: int, 
        output_size: int,
        dropout: float,
        use_selu: Optional[bool]=False
    ) -> None:
        super(MLP2, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.fc21 = nn.Linear(hidden_size, output_size)
        self.fc22 = nn.Linear(hidden_size, output_size)
        self.nonlinear_f = F.selu if use_selu else F.relu
        self.dropout = nn.Dropout(dropout)
    def forward(self, x: FloatTensor) -> Tuple[FloatTensor, FloatTensor]:
        h1 = self.dropout(self.nonlinear_f(self.fc1(x)))
        return self.fc21(h1), self.fc22(h1)

def init_linear(m):
    if isinstance(m, nn.Linear):
        nn.init.xavier_uniform_(m.weight)
        nn.init.zeros_(m.bias)

def logwsumexp(x, w):
    """
    log weighted sum (along dim 1) exp, i.e., log(sum(w * exp(x), 1)).

    Input:
      x (n, m): exponents
      w (n, m): weights
    Output:
      y (n,)
    """
    maxv, _ = torch.max(x, dim=1, keepdim=True)
    y = torch.log(torch.sum(torch.exp(x - maxv) * w, dim=1, keepdim=True)) + maxv
    return y.squeeze(1)

def IGlog_pdf(logμ, logλ, t):
    """
    log pdf of IG distribution.
    
    Args:
        logμ, logλ (batch_size,): the parameters of IG.
        t (batch_size,): the travel time.
    Output:
        logpdf (batch_size,)
    """
    eps = 1e-5
    μ = torch.exp(logμ)
    expt = -0.5 * torch.exp(logλ)*torch.pow(t-μ,2) / (μ.pow(2)*t+eps)
    logz = 0.5*logλ - 1.5*torch.log(t)
    return expt+logz

def IGlog_prob_loss(logμ, logλ, t):
    """
    Args:
        logμ, logλ (batch_size,): the parameters of IG.
        t (batch_size,): the travel time.
    Returns:
        The average loss of the log probability of a batch of data.
    """
    logpdf = IGlog_pdf(logμ, logλ, t)
    return torch.mean(-logpdf)

def smooth_l1_loss(logμ, logλ, t):
    μ = torch.exp(logμ)
    return F.smooth_l1_loss(μ, t, beta=2.0)

class ProbRho(nn.Module):
    def __init__(
        self, 
        num_u: int, 
        num_h: int,
        dim_u: int,
        dim_h: int,
        dim_ρ: int,
        hidden_size: int,
        hids: np.array,
        centroids: np.array,                                              
        dropout: float, use_selu: bool, device
    ) -> None:
        """
        num_h: the number of highway type.
        hids (1+num_edge,): mapping edge id to highway type id.
        lengths (1+num_edge,): edge length.
        centroids (1+num_edge, 2): the centroid coordinates of edges.
        """
        super(ProbRho, self).__init__()
        self.hids = torch.LongTensor(hids).to(device)
        self.centroids = torch.FloatTensor(centroids).to(device)

        self.embedding_u = nn.Embedding(num_u, dim_u)
        self.embedding_h = nn.Embedding(num_h, dim_h)

        self.f = MLP2(dim_u+dim_h,
                      hidden_size, 
                      dim_ρ, 
                      dropout, use_selu)
        
        self.f.apply(init_linear)
        
    def reparameterize(self, mu, logvar):
        if self.training:
            std = torch.exp(0.5*logvar)
            eps = torch.randn_like(std)
            return eps.mul(std).add(mu)
        else:
            return mu

    def forward(self, link: LongTensor) -> FloatTensor:
        """
        Args:
            link (batch_size, seq_len)
        Returns:
            ρ (batch_size, seq_len, dim_ρ)
        """
        ## (batch_size, seq_len, dim_u)
        u = self.embedding_u(link)
        ## (batch_size, seq_len, dim_h)
        h = self.embedding_h(self.hids[link])
        x = torch.cat([u, h], dim=2)
        mu, logvar = self.f(x)
        return self.reparameterize(mu, logvar)

class ProbTraffic(nn.Module):
    """
    Modelling the probability of the traffic state `c`
    """
    def __init__(
        self, 
        n_in: int, 
        dim_c: int, 
        hidden_size: int,
        dropout: float, 
        use_selu: bool
    ) -> None:
        super(ProbTraffic, self).__init__()
        conv_layers = [
            nn.Conv2d(n_in, 32, (5, 5), stride=2, padding=1),
            nn.BatchNorm2d(32),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Conv2d(32, 64, (4, 4), stride=2, padding=1),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Conv2d(64, 128, (3, 3), stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),
            nn.AvgPool2d(3)
        ]
        self.f1 = nn.Sequential(*conv_layers)
        ## input dimension equals to the dimension of the output of f1
        self.f2 = MLP2(128*2*3, hidden_size, dim_c, dropout, use_selu)

        self.f2.apply(init_linear)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = torch.exp(0.5*logvar)
            eps = torch.randn_like(std)
            return eps.mul(std).add(mu)
        else:
            return mu

    def forward(self, S: FloatTensor) -> Tuple[FloatTensor, FloatTensor, FloatTensor]:
        """
        Input:
          S (batch_size, nchannel, height, width)
        Output:
          c, mu, logvar (batch_size, dim_c)
        """
        x = self.f1(S)
        mu, logvar = self.f2(x.view(x.size(0), -1))
        return self.reparameterize(mu, logvar), mu, logvar


class ProbTravelTime(nn.Module):
    def __init__(
        self, 
        dim_ρ: int, 
        dim_c: int,
        dim_t: int, 
        hidden_size: int,
        dropout: float, 
        use_selu: bool
    ) -> None:
        super(ProbTravelTime, self).__init__()
        self.f = MLP2(dim_ρ+dim_c+dim_t, hidden_size, 1, dropout, use_selu)
        
        self.f.apply(init_linear)

    def forward(
        self,
        ρ: FloatTensor,
        c: FloatTensor,
        w: FloatTensor,
        l: FloatTensor
    ) -> Tuple[FloatTensor, FloatTensor]:
        """
        Args:
            ρ (batch_size, seq_len, dim_ρ)
            c (batch_size, dim_c): the traffic state vector sampling from ProbTraffic
            w (batch_size, seq_len): the normalized link lengths
            l (batch_size, ): route or path lengths
        Returns:
            logμ (batch_size,)
            logλ (batch_size,)
        """
        ## (batch_size, seq_len, dim_ρ+dim_c)
        x = torch.cat([ρ, c.unsqueeze(1).expand(-1, ρ.shape[1], -1)], 2)
        ## (batch_size, seq_len, 1)
        logm, logv = self.f(x)
        ## (batch_size, seq_len)
        logm, logv = logm.squeeze(2), logv.squeeze(2) 
        logl, logw = torch.log(l), torch.log(w)
        ## (batch_size, )
        #logm_agg = logwsumexp(logm, w)
        #logv_agg = logwsumexp(logv, w.pow(2))
        logm_agg = torch.logsumexp(logm + logw, dim=1)
        logv_agg = torch.logsumexp(logv + 2*logw, dim=1)
        ## parameters of IG distribution
        ## (batch_size, )
        logμ = logl - logm_agg
        logλ = logl - 3*logm_agg - logv_agg
        return logμ, logλ

class DeepGTT(nn.Module):
    def __init__(
        self, 
        num_u: int,
        num_h: int,
        dim_u: int,
        dim_h: int,
        dim_ρ: int,
        dim_c: int,
        dim_t: int,
        slot_size: int,
        n_in: int,
        hidden_size1: int,
        hidden_size2: int,
        hidden_size3: int,
        hids: np.array,
        lengths: np.array,
        centroids: np.array,                                              
        dropout: float, use_selu: bool, device
    ) -> None:
        super(DeepGTT, self).__init__()
        self.lengths = torch.FloatTensor(lengths).to(device)
        
        self.dim_c = dim_c
        self.dim_t = dim_t
        self.slot_size = slot_size
        num_slot = pipe(24*60 / slot_size, np.ceil, int)
        self.embedding_slot = nn.Embedding(num_slot, dim_t)
        
        self.probrho = ProbRho(
            num_u,
            num_h,
            dim_u,
            dim_h,
            dim_ρ,
            hidden_size1,
            hids,
            centroids,
            dropout,
            use_selu,
            device
        )
        self.probtraffic = ProbTraffic(
            n_in,
            dim_c,
            hidden_size2,
            dropout,
            use_selu
        )
        self.probttime = ProbTravelTime(
            dim_ρ,
            dim_c,
            dim_t,
            hidden_size3,
            dropout,
            use_selu
        )
    
    def forward(self, trainbatch: TrainBatch):
        device = self.lengths.device
        ## (batch_size, seq_len)
        cpath = trainbatch.cpath.to(device)
        ## (batch_size, 1)
        route_len = trainbatch.route_len.to(device)
        ## (batch_size, 2)
        ratio = trainbatch.ratio.to(device)
        ## (batch_size, seq_len)
        linklen = self.get_linklen(cpath, route_len, ratio)
        ## (batch_size, 1)
        l = linklen.sum(dim=1, keepdim=True)
        w = linklen / l
        ρ = self.probrho(cpath)
        #c, mu_c, logvar_c = self.probtraffic(trainbatch.S.to(device))
        t = self.get_t(trainbatch.stms)
        #x = torch.cat([c, t], dim=-1)
        logμ, logλ = self.probttime(ρ, t, w, l.squeeze())
        return torch.clamp_max(logμ, 10), torch.clamp_max(logλ, 10), 0, 0

    def get_linklen(self, cpath: LongTensor, route_len: FloatTensor, ratio: FloatTensor):
        ## (batch_size, seq_len)
        linklen = self.lengths[cpath]
        #index = torch.cat([torch.zeros_like(route_len), route_len-1], dim=1)
        #linklen.scatter_(1, index, torch.gather(linklen, 1, index) * ratio)
        return linklen

    def get_t(self, tms: List[float]):
        device = self.lengths.device
        k = pipe(tms, map(lambda tms: discretize_min(tms, self.slot_size)), list)
        return self.embedding_slot(torch.LongTensor(k).to(device))
    
Parameters = namedtuple("Parameters", [
    "num_u",
    "num_h",
    "dim_u",
    "dim_h",
    "dim_ρ",
    "dim_c",
    "dim_t",
    "slot_size",
    "n_in",
    "hidden_size1",
    "hidden_size2",
    "hidden_size3",
    "hids",
    "lengths",
    "centroids",
    "dropout",
    "use_selu",
    "device"
])