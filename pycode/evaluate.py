import torch
import torch.nn.functional as F
from datautils import get_dataloader, get_transform_batch
from model import DeepGTT, IGlog_prob_loss, Parameters

dirpath = "../mm/trips/output"
traffic_tensor_file = "../data/output/traffic_tensor.jld2"
model_name = "../data/output/best-model.pt"
model_dict = torch.load(model_name)
# parameters = Parameters(**model_dict["parameters"])
# model = DeepGTT(
#     parameters.num_u,
#     parameters.num_h,
#     parameters.dim_u,
#     parameters.dim_h,
#     parameters.dim_e,
#     parameters.dim_ρ,
#     parameters.dim_c,
#     parameters.dim_t,
#     parameters.slot_size,
#     parameters.n_in,
#     parameters.hidden_size1,
#     parameters.hidden_size2,
#     parameters.hidden_size3,
#     parameters.hids,
#     parameters.lengths,
#     parameters.centroids,
#     parameters.dropout, 
#     parameters.use_selu,
#     device
# ).to(device)
device = model_dict["parameters"]["device"]
model = DeepGTT(**model_dict["parameters"]).to(device)

model.load_state_dict(model_dict["model"])

test_names = ["gps_20161108.jld2"]
test_dataloader = get_dataloader(dirpath, test_names, 500, False)
transform_batch = get_transform_batch(traffic_tensor_file)

@torch.no_grad()
def validate(dataloader):
    model.eval()
    total_loss, total_mae = 0, 0
    dataloader.create_batches()
    for i, batch in enumerate(dataloader.batches):
        trainbatch = transform_batch(batch)
        logμ, logλ, _, _ = model(trainbatch)
        time = trainbatch.time.to(device)
        loss = IGlog_prob_loss(logμ, logλ, time)
        mae = F.l1_loss(torch.exp(logμ), time)
        total_loss += loss.item()
        total_mae += mae.item()
    mean_loss, mean_mae = total_loss/(i+1), total_mae/(i+1)
    print(f"Validation Loss: {mean_loss:.4f} MAE: {mean_mae:.4f}")
    return mean_loss, mean_mae

validate(test_dataloader)