import torch
import torch.nn.functional as F
import numpy as np
import geopandas as gpd
import argparse, time
from torch.nn.utils import clip_grad_norm_
from datautils import (get_dataloader, get_transform_batch,
                       get_highwayids, get_centroids, get_lengths)
from model import DeepGTT, IGlog_prob_loss, smooth_l1_loss, Parameters


parser = argparse.ArgumentParser(description="train.py")
parser.add_argument("-dirpath", type=str,
    default="../mm/trips/output")
parser.add_argument("-osm_data", type=str,
    default="../mm/entrance/data/cities/chengdu/edges.shp")
parser.add_argument("-traffic_tensor_file", type=str,
    default="../data/output/traffic_tensor.jld2")
parser.add_argument("-dim_u", type=int, default=200)
parser.add_argument("-dim_h", type=int, default=16)
parser.add_argument("-dim_ρ", type=int, default=256)
parser.add_argument("-dim_c", type=int, default=0)
parser.add_argument("-dim_t", type=int, default=100)
parser.add_argument("-hidden_size1", type=int, default=200)
parser.add_argument("-hidden_size2", type=int, default=200)
parser.add_argument("-hidden_size3", type=int, default=200)
parser.add_argument("-slot_size", type=int, default=10)
parser.add_argument("-n_in", type=int, default=1)
parser.add_argument("-batch_size", type=int, default=150)
parser.add_argument("-num_epoch", type=int, default=10)
parser.add_argument("-dropout", type=float, default=0.2)
parser.add_argument("-max_grad_norm", type=float, default=1.0)
parser.add_argument("-lr", type=float, default=0.001)
parser.add_argument("-lr_decay", type=float, default=0.2)
parser.add_argument("-kl_decay", type=float, default=0.0)
parser.add_argument("-use_selu", action="store_true")
args = parser.parse_args()

np.random.seed(123)
torch.manual_seed(123)
device = torch.device("cuda:0")
edges = gpd.read_file(args.osm_data)
hids, num_h = get_highwayids(edges)
centroids = get_centroids(edges)
lengths = get_lengths(edges)
num_u = len(lengths)

parameters = Parameters(
    num_u=num_u,
    num_h=num_h,
    dim_u=args.dim_u,
    dim_h=args.dim_h,
    dim_ρ=args.dim_ρ,
    dim_c=args.dim_c,
    dim_t=args.dim_t,
    slot_size=args.slot_size,
    n_in=args.n_in,
    hidden_size1=args.hidden_size1,
    hidden_size2=args.hidden_size2,
    hidden_size3=args.hidden_size3,
    hids=hids,
    lengths=lengths,
    centroids=centroids,
    dropout=args.dropout,
    use_selu=args.use_selu,
    device=device
)

model = DeepGTT(
    num_u,
    num_h,
    args.dim_u,
    args.dim_h,
    args.dim_ρ,
    args.dim_c,
    args.dim_t,
    args.slot_size,
    args.n_in,
    args.hidden_size1,
    args.hidden_size2,
    args.hidden_size3,
    hids,
    lengths,
    centroids,
    args.dropout, 
    args.use_selu,
    device
).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, amsgrad=True)

train_names = ["gps_20161101.jld2", "gps_20161102.jld2", "gps_20161103.jld2", 
               "gps_20161104.jld2", "gps_20161105.jld2", "gps_20161106.jld2"]
#train_names = ["gps_20161104.jld2"]
valid_names = ["gps_20161107.jld2"]
train_dataloader = get_dataloader(args.dirpath, train_names, args.batch_size, True)
valid_dataloader = get_dataloader(args.dirpath, valid_names, 500, False)
transform_batch = get_transform_batch(args.traffic_tensor_file)

def adjust_lr(epoch, lr, lr_decay):
    lr = lr * (lr_decay ** (epoch//3))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def GaussianKLD(mu, logvar):
    return -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp()) / mu.shape[0]

lossF = IGlog_prob_loss

@torch.no_grad()
def validate(dataloader):
    model.eval()
    total_loss, total_mae = 0, 0
    dataloader.create_batches()
    for i, batch in enumerate(dataloader.batches):
        trainbatch = transform_batch(batch)
        logμ, logλ, _, _ = model(trainbatch)
        time = trainbatch.time.to(device)
        loss = lossF(logμ, logλ, time)
        mae = F.l1_loss(torch.exp(logμ), time)
        total_loss += loss.item()
        total_mae += mae.item()
    mean_loss, mean_mae = total_loss/(i+1), total_mae/(i+1)
    print(f"Validation Loss: {mean_loss:.4f} MAE: {mean_mae:.4f}")
    model.train()
    return mean_loss, mean_mae


def train(num_epoch: int, max_grad_norm: float, β: float):
    def train_step(batch):
        trainbatch = transform_batch(batch)
        logμ, logλ, mu_c, logvar_c = model(trainbatch)
        time = trainbatch.time.to(device)
        loss = lossF(logμ, logλ, time) #+ β * GaussianKLD(mu_c, logvar_c)
        optimizer.zero_grad()
        loss.backward()
        #clip_grad_norm_(model.parameters(), max_grad_norm)
        optimizer.step()

        mae = F.l1_loss(torch.exp(logμ), time)
        return loss.item(), mae.item()
    
    min_mae = 1e9
    for epoch in range(num_epoch):
        epoch_loss, epoch_mae, running_mae = 0, 0, 0
        train_dataloader.create_batches()
        for i, batch in enumerate(train_dataloader.batches):
            loss, mae = train_step(batch)
            epoch_loss += loss
            epoch_mae += mae
            running_mae += mae
            if (i + 1) % 500 == 0:
                print(f"Epoch: {epoch:3} Running mae: {running_mae/(i+1):.4f}")
        print(f"Epoch: {epoch:3} Epoch Loss: {epoch_loss/i:.4f} Epoch MAE: {epoch_mae/i:.4f}")
        _, mean_mae = validate(valid_dataloader)
        if mean_mae < min_mae:
            print(f"Saving model at epoch {epoch:3}, mae: {mean_mae:.4f}.")
            torch.save({
                "model": model.state_dict(),
                "parameters": parameters._asdict()
            }, "../data/output/best-model.pt")
            min_mae = mean_mae
        #adjust_lr(epoch, args.lr, args.lr_decay)

tic = time.time()
train(args.num_epoch, args.max_grad_norm, args.kl_decay)
print(f"Time passed: {(time.time() - tic)/3600} hours.")

### testing
test_names = ["gps_20161108.jld2"]
test_dataloader = get_dataloader(args.dirpath, test_names, 500, False)
model_dict = torch.load("../data/output/best-model.pt")
model.load_state_dict(model_dict["model"])
validate(test_dataloader)
